'use strict';

const {compress, determineOrientation} = require('jpegasus');
const Camera = require('./camera');
const heic2any = require('heic2any');

let initialized = false;
window.addEventListener('load', async () => {
  if (initialized) {
    return;
  }
  initialized = true;

  const camera = new Camera(
    document.querySelector('video'),
    document.querySelector('select#videoSource')
  );

  document.getElementById('uploadImageButton').addEventListener('click', uploadImage);
  document.getElementById('cameraStart').addEventListener('click', async (event) => {
    event.stopPropagation();
    event.preventDefault();
    await camera.getWebcam();
  });

  document.querySelector('select#videoSource').addEventListener('change', async () => {
    await camera.getStream();
  });

  document.getElementById('cameraSnap').addEventListener('click', async (event) => {
    event.stopPropagation();
    event.preventDefault();
    const imageData = camera.snap();

    if (imageData) {
      const file = base64ToFile(imageData);
      compressFile(file);
    }
  });
});

async function getFileFromHeic(file) {
  const data = await file.arrayBuffer();
  const blob = new Blob([data]);

  const converted = await heic2any({
    blob,
    toType: "image/jpeg",
    quality: 0.7, // cuts the quality and size by half
  });

  return blobToFile(converted);
}


async function uploadImage(event) {
  event.stopPropagation();
  event.preventDefault();

  const file = document.getElementById('fileInput').files[0];

  if (file && !file.type.match(/hei[cf]/)) {
    compressFile(file);
  } else {
    const convertedFile = await getFileFromHeic(file);
    compressFile(convertedFile);
  }

};

const setFileObjectUrl = (compressedFile) => {
  const compressedImageSource = URL.createObjectURL(compressedFile);
  document.getElementById('imagePreview').innerHTML = `<img id="compressedImage" src="${compressedImageSource}" />`;
};

async function compressFile(file) {
  console.info('originalFile:', file)

  const maxHeight = 4048;
  const maxWidth = 4048;
  const quality = 0.7;
  const returnOriginalIfCompressedFileIsLarger = true;
  const returnOriginalOnFailure = true;
  // TODO: image orientation fixing does not work, need to figure out why
  const fixImageOrientation = false;
  const preserveFileType = false;
  const transparencyFillColor = '#fff';
  const compressedFile = await compress(file, {
    fixImageOrientation,
    maxHeight,
    maxWidth,
    preserveFileType,
    quality,
    returnOriginalIfCompressedFileIsLarger,
    returnOriginalOnFailure,
    transparencyFillColor,
  });
  console.info('compressedFile:', compressedFile)
  document.querySelector('#imageInformation').innerHTML = `
  Compressed size: ${compressedFile.size}
  Orginal size:    ${file.size}`;
  setFileObjectUrl(compressedFile);
}

function base64ToFile(imageData) {
  var BASE64_MARKER = ';base64,';
  var mime = imageData.split(BASE64_MARKER)[0].split(':')[1];
  var filename = 'dataURI-file-' + (new Date()).getTime() + '.' + mime.split('/')[1];
  var bytes = atob(imageData.split(BASE64_MARKER)[1]);
  var writer = new Uint8Array(new ArrayBuffer(bytes.length));

  for (var i=0; i < bytes.length; i++) {
    writer[i] = bytes.charCodeAt(i);
  }

  return new File([writer.buffer], filename, { type: mime });
}

function blobToFile(blob){
  var filename = 'dataURI-file-' + (new Date()).getTime() + '.' + blob.type;
  blob.lastModifiedDate = new Date();
  blob.name = filename;
  return blob;
}
