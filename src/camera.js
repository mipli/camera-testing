module.exports = class Camera {
  constructor(videoElement, sourceElement) {
    this.previewWidth = 400;
    this.videoElement = videoElement;
    this.sourceElement = sourceElement;
    this.stream = null;
    this.isActive = false;
  }

  async getWebcam() {
    await this.getStream();
    const deviceInfos = await this.getDevices();
    await this.showDevices(deviceInfos);
  }

  getDevices() {
    return navigator.mediaDevices.enumerateDevices();
  }

  showDevices(deviceInfos) {
    console.log('Available devices:', deviceInfos);
    while (this.sourceElement.firstChild) {
      this.sourceElement.removeChild(this.sourceElement.firstChild);
    }
    for (const deviceInfo of deviceInfos) {
      if (deviceInfo.kind === 'videoinput') {
        const option = document.createElement('option');
        option.value = deviceInfo.deviceId;
        option.text = deviceInfo.label || `Camera ${this.sourceElement.length + 1}`;
        this.sourceElement.appendChild(option);
      }
    }
  }

  async getStream() {
    const videoSource = this.sourceElement.value;
    const constraints = {
      video: {
        deviceId: videoSource ? {exact: videoSource} : undefined,
        width: 1920,
        height: 1080,
      }
    };
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    this.showStream(stream);
  }

  showStream(stream) {
    this.stopStreams();
    this.isActive = true;

    this.stream = stream;
    this.sourceElement.selectedIndex = [...this.sourceElement.options].
      findIndex(option => option.text === stream.getVideoTracks()[0].label);
    this.videoElement.srcObject = stream;
    this.videoElement.muted = true;

    this.videoElement.addEventListener('canplay', () => {
      const scale = this.previewWidth / this.videoElement.videoWidth;
      this.videoElement.style.transform = `scale(${scale})`;
      this.videoElement.style.transformOrigin = '0 0';
    });
  }

  stopStreams() {
    this.isActive = false;
    if (this.stream) {
      this.stream.getTracks().forEach(track => {
        track.stop();
      });
    }
    this.videoElement.srcObject = null;
  }

  snap() {
    if (!this.isActive) {
      return null;
    }

    const canvas = document.createElement('canvas');
    canvas.height = this.videoElement.scrollHeight;
    canvas.width = this.videoElement.scrollWidth;
    const context = canvas.getContext('2d');
    context.drawImage(this.videoElement, 0, 0, canvas.width, canvas.height);
    return canvas.toDataURL('image/jpg');
  }
}
