const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  mode: 'development',
  output: {
    filename: '[name].bundle.js',
    path: path.resolve(__dirname, 'dist'),
  },
  plugins: [
    new HtmlWebpackPlugin({
      hash: true,
      title: 'Development',
      template: './src/index.html'
    }),
  ],
  devServer: {
    https: true,
    hot: true,
    contentBase: path.join(__dirname, 'dist'),
    port: 9000,
  },
};
